import React, {useState, useEffect} from 'react'
import SectionHeader from '../SectionHeader';

import "./appointment.css"

const PatientAppointment = () => {

    // FETCHING DATA FROM DB
    const [appointment, setAppointment] = useState([]);

    // FETCHING PATIENT APPOINTMENT
    const getPatientData = async () => {
        try {
            const res = await fetch("/getPatientAppointment", {
                method: "GET",
                headers: { 'Content-Type': 'application/json' }
            });
            const getPatient = await res.json();
            // console.log(getPatient);
            setAppointment(getPatient);

            if (!res.status === 200) {
                const error = new Error(res.error);
                throw error;
            }
        } catch (err) {
            console.log(err);
        }
    }

    useEffect(() => {
        getPatientData();
    }, []);

    return (
        <>
            <section id="patient-appointment-section">
                <form method="GET">
                    <div className="container-fluid">
                        <div className="content-box-md">
                            <div className="row">
                                <div className="col-md-10 col-sm-3 mx-auto">
                                    <div className="row">
                                        <SectionHeader title="Patient Appointment" />
                                        <div className="col-md-12 col-xs-5 mx-auto">
                                            <div className="row">
                                                <div className="col-md-12 mx-auto">
                                                    <div className="d-flex flex-wrap justify-content-center">
                                                        {
                                                            appointment.map((item, index) => (
                                                                <div className="approval-card" key={index}>
                                                                    <h4 className="patient-name mb-4">Patient Name: {item.pname}</h4>
                                                                    <p className="approved" hidden>Doctor: {item.doctor}</p>
                                                                    <p className="experience">
                                                                        Priority: {item.priority}
                                                                    </p>
                                                                    <p className="designation">
                                                                        Date: {item.date}
                                                                    </p>
                                                                    <p className="location">Time: {item.time} </p>
                                                                    <p className="location">Mode: {item.mode} </p>
                                                                </div>
                                                            ))
                                                        }
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </section> 
        </>
    )
}

export default PatientAppointment;
