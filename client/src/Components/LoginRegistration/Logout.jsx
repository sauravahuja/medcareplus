import React, { useEffect, useContext } from 'react'
import { useHistory } from 'react-router-dom';

import { UserContext } from "../../App"

const Logout = () => {

    const {state, dispatch} = useContext(UserContext);

    useEffect(() => {
        const callLogOut = async () => {
            const res = await fetch('/logout', {
                method: "GET",
                headers: {
                    Accept: "application/json", 
                    "Content-Type": "application/json",
                },
                credentials: "include" 
            })
            const data = await res.json();
            
            if(!data.isError) {
                dispatch({type: "USER", payload: false});
            }
        }
        callLogOut();
    }, [])

    
    return (
        <div>
            <h1>Logout Successfully</h1>
        </div>
    )
}

export default Logout
